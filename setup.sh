#/bin/bash


git clone https://gitlab.com/toggle-corp/covid-19-records ~/covid-19-records

cd ~/covid-19-records && virtualenv venv && source venv/bin/activate && pip install -r requirements.txt

python manage.py migrate
