from django.contrib import admin

from .models import Country, Hospital, District, InfectionRecord


@admin.register(InfectionRecord)
class InfectionRecordAdmin(admin.ModelAdmin):
    exclude = ('created_at', 'modified_at')
    autocomplete_fields = ('district', 'infection_origin')


@admin.register(Hospital)
class HospitalAdmin(admin.ModelAdmin):
    autocomplete_fields = ('district',)


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    search_fields = ('name', 'province')


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    search_fields = ('name',)
