from django.db import models


class District(models.Model):
    name = models.CharField(max_length=25, unique=True)
    province = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Hospital(models.Model):
    name = models.CharField(max_length=50)
    district = models.ForeignKey(District, null=True, on_delete=models.SET_NULL)
    location = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.name


class InfectionRecord(models.Model):
    STATUS_TREATMENT_ONGOING = 'treatment_ongoing'
    STATUS_RECOVERED = 'recovered'
    STATUS_DEATH = 'death'

    CHOICES_STATUS = (
        (STATUS_TREATMENT_ONGOING, 'Treatment ongoing'),
        (STATUS_RECOVERED, 'Recovered'),
        (STATUS_DEATH, 'death')
    )

    GENDER_MALE = 'male'
    GENDER_FEMALE = 'female'
    GENDER_NO_DISCLOSURE = 'no_disclosure'

    CHOICES_GENDER = (
        (GENDER_MALE, 'Male'),
        (GENDER_FEMALE, 'Female'),
        (GENDER_NO_DISCLOSURE, 'No Disclosure'),
    )

    person_name = models.CharField(max_length=50, blank=True, null=True)
    gender = models.CharField(max_length=20, null=True, blank=True, choices=CHOICES_GENDER)
    age = models.PositiveIntegerField(null=True, blank=True)
    reported_date = models.DateTimeField(null=True, blank=True)
    infection_origin = models.ForeignKey(Country, null=True, blank=True, on_delete=models.SET_NULL)
    district = models.ForeignKey(District, null=True, blank=True, on_delete=models.SET_NULL)
    address = models.CharField(max_length=50, blank=True, null=True)
    treatment_at = models.ForeignKey(Hospital, null=True, blank=True, on_delete=models.SET_NULL)
    person_status = models.CharField(max_length=30, null=True, blank=True, choices=CHOICES_STATUS)
    information_source = models.TextField(null=True, blank=True)
    situation_report = models.TextField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
